// Written in the D programming language

/++
    These are the bindings for libdiscid from MusicBrainz.

    Libdiscid is a C library for calculating DiscIDs
    ($(HTTP musicbrainz.org/doc/Disc ID, MusicBrainz)
    and $(HTTP freedb.org, freedb)) for Audio CDs.
    Additionally the library can extract the MCN/UPC/EAN and the
    $(HTTP musicbrainz.org/doc/ISRC, ISRCs) from a disc.

    The idea is to have an easy to use library without any dependencies
    that can be used from scripting languages.

    This is an example of the most basic usage:
    ---
    auto disc = discid_new();
    scope(exit) discid_free(disc);

    if(discid_read_sparse(disc, "/dev/cdrom", 0) == 0)
    {
        stderr.writefln("Error: %s", discid_get_error_msg(disc).fromStringz());
        return 1;
    }

    writefln("DiscID     : %s", discid_get_id(disc));
    writefln("Submit via : %s", discid_get_submission_url(disc));
    ---

    Copyright: Copyright 2018
    License:   $(HTTPS www.gnu.org/licenses/lgpl-2.1.html, LGPL 2.1 or later).
    Authors:   $(HTTPS jmdavisprog.com, Jonathan M Davis)
    Source:    $(LINK_TO_SRC _discid.d)

    See_Also: $(LINK https://musicbrainz.org/doc/libdiscid)
  +/
module discid;

extern(C):


unittest
{
    import std.string : fromStringz;
    import std.stdio : writeln, writefln;

    int firstTrack;
    int lastTrack;
    int sectors;
    int[] trackOffsets;

    {
        writefln("Default device: %s", discid_get_default_device().fromStringz());
        writefln("libdiscid version: %s", discid_get_version_string().fromStringz());
        writefln("Platform has DISCID_FEATURE_READ: %s", discid_has_feature(DISCID_FEATURE_READ));
        writefln("Platform has DISCID_FEATURE_MCN: %s", discid_has_feature(DISCID_FEATURE_MCN));
        writefln("Platform has DISCID_FEATURE_ISRC: %s", discid_has_feature(DISCID_FEATURE_ISRC));

        {
            writeln("Features:");
            char*[DISCID_FEATURE_LENGTH] features;
            discid_get_feature_list(features);
            foreach(i, feature; features)
                writefln("   %s: %s", i, feature is null ? "null" : feature.fromStringz());
        }

        foreach(i; 0 .. 3)
        {
            auto disc = discid_new();
            scope(exit) discid_free(disc);

            if(i == 0)
            {
                writeln("\nsparse read");
                writeln("----------");
                if(discid_read_sparse(disc, null, 0) == 0)
                {
                    writefln("Failed to read disc: %s", discid_get_error_msg(disc).fromStringz());
                    return;
                }
            }
            else if(i == 1)
            {
                writeln("\nfull read");
                writeln("---------");
                if(discid_read(disc, null) == 0)
                {
                    writefln("Failed to read disc: %s", discid_get_error_msg(disc).fromStringz());
                    return;
                }
            }
            else
            {
                writeln("\nfrom put");
                writeln("--------");

                int[100] offsets;
                offsets[0] = sectors;
                offsets[1 .. lastTrack + 2 - firstTrack] = trackOffsets[firstTrack .. lastTrack + 1];

                if(discid_put(disc, firstTrack, lastTrack, offsets.ptr) == 0)
                {
                    writefln("discid_put failed: %s", discid_get_error_msg(disc).fromStringz());
                    return;
                }
            }

            sectors = discid_get_sectors(disc);

            writefln("Disc ID: %s", discid_get_id(disc).fromStringz());
            writefln("FreeDB Disc ID: %s", discid_get_freedb_id(disc).fromStringz());
            writefln("TOC String: %s", discid_get_toc_string(disc).fromStringz());
            writefln("Submission URL: %s", discid_get_submission_url(disc).fromStringz());
            writefln("Length in Sectors: %s", sectors);
            writefln("MCN: %s", discid_get_mcn(disc).fromStringz());

            firstTrack = discid_get_first_track_num(disc);
            lastTrack = discid_get_last_track_num(disc);
            writefln("First Track#: %s", firstTrack);
            writefln("Last Track#: %s\n", lastTrack);

            if(i == 1)
                trackOffsets.length = lastTrack + 1;

            writeln("Tracks:");
            foreach(track; firstTrack .. lastTrack + 1)
            {
                immutable offset = discid_get_track_offset(disc, track);
                writefln("    %02d: Offset: %s", track, offset);
                writefln("        Length: %s", discid_get_track_length(disc, track));
                writefln("        ISRC: %s",  discid_get_track_isrc(disc, track));

                if(i == 1)
                    trackOffsets[track] = offset;
            }
        }
    }

    // So that there's an empty line before output from the ddoc-ed unittest block for discid_put.
    writeln("");
}


///
enum DISCID_VERSION_MAJOR = 0;
///
enum DISCID_VERSION_MINOR = 6;
///
enum DISCID_VERSION_PATCH = 2;
///
enum DISCID_VERSION_NUM = 602;


/++
    A transparent handle for an Audio CD.

    This is returned by $(LREF discid_new) and has to be passed as the first
    parameter to all discid_*() functions.
  +/
alias DiscId = void*;


/++
    Return a handle for a new $(LREF DiscId) object.

    If no memory could be allocated, null is returned. Don't use the created
    $(LREF DiscId) object before calling $(LREF discid_read) or
    $(LREF discid_put).

    Returns: a $(LREF DiscId) object, or null.
  +/
DiscId* discid_new() @trusted nothrow @nogc;


/++
    Release the memory allocated for the $(LREF DiscId) object.

    Params:
       d = a $(LREF DiscId) object created by $(LREF discid_new)
  +/
void discid_free(scope DiscId* d) @trusted nothrow @nogc;


/++
    Read all supported features of the disc in the given CD-ROM/DVD-ROM drive.

    This function reads the disc in the drive specified by the given device
    identifier. If the device is null, the default drive, as returned by
    $(LREF discid_get_default_device) is used.

    If you do not require all features provided by libdiscid, such as MCN or
    ISRC reading, you should consider using $(LREF discid_read_sparse) instead
    of $(LREF _discid_read) for performance reasons.

    On error, this function returns $(K_FALSE) and sets the error message which
    you can access using $(LREF discid_get_error_msg). In this case, the other
    functions won't return meaningful values and should not be used.

    This function may be used multiple times with the same $(LREF DiscId)
    object.

    Params:
       d = a $(LREF DiscId) object created by $(LREF discid_new)
       device = an operating system dependent device identifier, or null

    Returns: $(K_TRUE) successful, or $(K_FALSE) on error.
  +/
int discid_read(scope DiscId* d, scope const char* device) @trusted nothrow @nogc;


/++
    Read the disc in the given CD-ROM/DVD-ROM drive extracting only the TOC and
    additionally specified features.

    This function will always read the TOC, but additional features like
    $(LREF DISCID_FEATURE_MCN) and $(LREF DISCID_FEATURE_ISRC) can be set using
    the features parameter. Multiple features can be set using bitwise OR.

    If you only want to generate a disc ID, you only need the TOC, so set
    features to 0:

    ---
    discid_read_sparse(disc, device, 0)
    ---

    This is a bit more verbose, but equivalent since $(LREF DISCID_FEATURE_READ)
    is always implied:

    ---
    discid_read_sparse(disc, device, DISCID_FEATURE_READ)
    ---

    If you want to read all features available, you can use $(LREF discid_read).

    On error, this function returns $(K_FALSE) and sets the error message which
    you can access using $(LREF discid_get_error_msg). In this case, the other
    functions won't return meaningful values and should not be used.

    This function may be used multiple times with the same $(LREF DiscId)
    object.

    Params:
        d = a DiscId object created by $(LREF discid_new)
        device = an operating system dependent device identifier, or null
        features = a list of bit flags from the DISCID_FEATURE_* enums.

    Returns: $(K_TRUE) if successful, or $(K_FALSE) on error.
  +/
int discid_read_sparse(scope DiscId* d, scope const char* device, uint features) @trusted nothrow @nogc;


/++
    Provides the TOC of a known CD.

    This function may be used if the TOC has been read earlier and you
    want to calculate the disc ID afterwards, without accessing the disc
    drive. It replaces the $(LREF discid_read) function in this case.

    On error, this function returns $(K_FALSE) and sets the error message which
    you can access using $(LREF discid_get_error_msg). In this case, the other
    functions won't return meaningful values and should not be used.

    The offsets parameter points to a C array containing the track offsets for
    each track. It must contain 100 elements. Rather than a track offset,
    $(D offsets[0]) contains the length of the sectors on the disc (see
    $(LREF discid_get_sectors)) and $(D offsets[1 .. numTracks + 1]) contains
    the actual track offsets. Unfortunately, the C documentation does not state
    what the remaining offsets should be, but having them be $(D 0) seems
    sensible and has worked in all of the cases tested (it's also what naturally
    happens if $(D int[100]) or $(D new int[100]) is used for the offsets).

    For discs with additional data tracks, the trailing data tracks
    should be ignored. $(D offsets[0]) should therefore be the last sector of
    the last audio track.

    Make sure the length of the last audio track as returned by libdiscid
    after a put is the same as the length of your last audio track.
    Depending on your tools you might need to substract 11400 (2:32 min.).

    Params:
        d = a DiscID object created by $(LREF discid_new)
        first = the number of the first audio track on disc (usually one)
        last = the number of the last audio track on the disc
        offsets = a pointer to a C array of 100 track offsets

    Returns: $(K_TRUE) if the given data was valid, and $(K_FALSE) on error

    See_Also: $(HTTP musicbrainz.org/doc/Disc_ID_Calculation, Disc ID Calculation)
  +/
int discid_put(scope DiscId* d, int first, int last, scope int* offsets) @trusted pure nothrow @nogc;

///
unittest
{
    import std.string : fromStringz;
    import std.stdio : writefln;

    int firstTrack;
    int lastTrack;
    int sectors;
    int[] trackOffsets;

    {
        auto disc = discid_new();
        scope(exit) discid_free(disc);

        if(discid_read_sparse(disc, null, 0) == 0)
        {
            writefln("Failed to read disc: %s", discid_get_error_msg(disc).fromStringz());
            return;
        }

        firstTrack = discid_get_first_track_num(disc);
        lastTrack = discid_get_last_track_num(disc);
        sectors = discid_get_sectors(disc);

        foreach(track; firstTrack .. lastTrack + 1)
            trackOffsets ~= discid_get_track_offset(disc, track);
    }

    auto disc = discid_new();
    scope(exit) discid_free(disc);

    int[100] offsets;
    offsets[0] = sectors;
    offsets[1 .. trackOffsets.length + 1] = trackOffsets[];

    if(discid_put(disc, firstTrack, lastTrack, &offsets[0]) == 0)
    {
        writefln("discid_put failed: %s", discid_get_error_msg(disc).fromStringz());
        return;
    }

    writefln("Disc ID: %s", discid_get_id(disc).fromStringz());
}


/++
    Return a human-readable error message.

    This function may only be used if $(LREF discid_read) failed. The returned
    error message is only valid as long as the $(LREF DiscId) object exists.

    Params:
        d = a $(LREF DiscId) object created by $(LREF discid_new)

    Returns: a string describing the error that occurred
  +/
char* discid_get_error_msg(scope DiscId* d) @trusted nothrow @nogc;


/++
    Return a MusicBrainz DiscID.

    The returned string is only valid as long as the $(LREF DiscId) object
    exists.

    Params:
        d = a $(LREF DiscId) object created by $(LREF discid_new)

    Returns: a C-string containing a MusicBrainz DiscID
  +/
char* discid_get_id(scope DiscId* d) @trusted pure nothrow @nogc;


/++
    Return a FreeDB DiscID.

    The returned string is only valid as long as the $(LREF DiscId) object
    exists.

    Params:
        d = a $(LREF DiscId) object created by $(LREF discid_new)

    Returns: a C-string containing a FreeDB DiscID
  +/
char* discid_get_freedb_id(scope DiscId* d) @trusted pure nothrow @nogc;


/++
    Return a string representing CD Table Of Contents (TOC).

    The string has following values separated by space:
    $(UL
        $(LI first track number)
        $(LI last track number)
        $(LI total length in sectors)
        $(LI offset of 1st track)
        $(LI offset of 2nd track))

    Example:
        $(D_CODE_STRING "1 7 164900 150 22460 50197 80614 100828 133318 144712")

    The returned string is only valid as long as the DiscId object exists.

    Params:
        d = a $(LREF DiscId) object created by $(LREF discid_new)

    Returns: a C-string containing TOC information
  +/
char* discid_get_toc_string(scope DiscId* d) @trusted pure nothrow @nogc;


/++
    Return a URL for submitting the DiscID to MusicBrainz.

    The URL leads to an interactive disc submission wizard that guides the
    user through the process of associating this disc's DiscID with a
    release in the MusicBrainz database.

    The returned string is only valid as long as the $(LREF DiscId) object
    exists.

    Params:
        d = a $(LREF DiscId) object created by $(LREF discid_new)

    Returns: a C-string containing a URL
  +/
char* discid_get_submission_url(scope DiscId* d) @trusted nothrow @nogc;


/++
    Return the name of the default disc drive for this machine. This isn't
    constant but possibly depends on the drives currently attached, depending
    on the platform. For this reason, you should call this once and save it when
    you want to make sure to use the same drive for multiple operations.

    The returned string is thread local and owned by libdiscid internally.

    Returns:
        a C-string containing an operating system dependent device identifier
  +/
char* discid_get_default_device() @trusted nothrow @nogc;


/++
    Return the number of the first track on this disc.

    Params:
        d = a $(LREF DiscId) object created by $(LREF discid_new)

    Returns: the number of the first track
  +/
int discid_get_first_track_num(scope DiscId* d) @trusted pure nothrow @nogc;


/++
    Return the number of the last audio track on this disc.

    Params:
        d = a $(LREF DiscId) object created by $(LREF discid_new)

    Returns: the number of the last track
  +/
int discid_get_last_track_num(scope DiscId* d) @trusted pure nothrow @nogc;


/++
    Return the length of the disc in sectors.

    Params:
        d = a $(LREF DiscId) object created by $(LREF discid_new)

    Returns: the length of the disc in sectors
  +/
int discid_get_sectors(scope DiscId* d) @trusted pure nothrow @nogc;


/++
    Return the sector offset of a track.

    Only track numbers between (and including)
    $(LREF discid_get_first_track_num) and $(LREF discid_get_last_track_num) may
    be used.

    Params:
        d = a $(LREF DiscId) object created by $(LREF discid_new)
        track_num = the number of a track

    Returns: the sector offset of the specified track
  +/
int discid_get_track_offset(scope DiscId* d, int track_num) @trusted pure nothrow @nogc;


/++
    Return the length of a track in sectors.

    Only track numbers between (and including)
    $(LREF discid_get_first_track_num) and $(LREF discid_get_last_track_num) may
    be used.

    Params:
        d = a $(LREF DiscId) object created by $(LREF discid_new)
        track_num = the number of a track

    Returns: the length of the specified track
  +/
int discid_get_track_length(scope DiscId* d, int track_num) @trusted pure nothrow @nogc;


/++
    Return the Media Catalogue Number (MCN) for the disc.

    This is essentially an EAN (= UPC with 0 prefix).

    Params:
        d = a $(LREF DiscId) object created by $(LREF discid_new)

    Returns: a C-string containing the Media Catalogue Number of the disc
  +/
char* discid_get_mcn(scope DiscId* d) @trusted pure nothrow @nogc;


/++
    Return the ISRC for a track.

    Only track numbers between (and including)
    $(LREF discid_get_first_track_num) and $(LREF discid_get_last_track_num) may
    be used.

    Params:
        d = a $(LREF DiscId) object created by $(LREF discid_new)
        track_num = the number of a track

    Returns: a C-string containing an ISRC for the specified track
  +/
char* discid_get_track_isrc(scope DiscId* d, int track_num) @trusted pure nothrow @nogc;


/++
    PLATFORM-DEPENDENT FEATURES

    The platform dependent features are currently:
    $(TABLE
        $(TR $(TD "read") $(TD read TOC from disc))
        $(TR $(TD "mcn") $(TD read MCN from disc))
        $(TR $(TD "isrc") $(TD ead ISRC from disc)))

    A table in the
    $(HTTP musicbrainz.org/doc/libdiscid, MusicBrainz Documentation)
    specifies which features are available on which platform in what version.

    In the code you can use $(LREF discid_get_feature_list) or
    $(LREF discid_has_feature) below to get the features for your platform in
    this version.
  +/
enum uint DISCID_FEATURE_READ = 1 << 0;
/// Ditto
enum uint DISCID_FEATURE_MCN  = 1 << 1;
/// Ditto
enum uint DISCID_FEATURE_ISRC = 1 << 2;


/++
    Check if a certain feature is implemented on the current platform.

    This only works for single features, not bit masks with multiple features.

    Params:
        feature = One of the DISCID_* enums

    Returns: $(D 1) if the feature is implemented and $(D 0) if not.

    See_ALSO: $(LREF DISCID_FEATURE_READ)$(BR)
              $(LREF DISCID_FEATURE_MCN)$(BR)
              $(LREF DISCID_FEATURE_ISRC)
  +/
int discid_has_feature(uint feature) @trusted pure nothrow @nogc;


///
enum DISCID_FEATURE_STR_READ = "read";
///
enum DISCID_FEATURE_STR_MCN  = "mcn";
///
enum DISCID_FEATURE_STR_ISRC = "isrc";
///
enum DISCID_FEATURE_LENGTH   = 32;


/++
    Return a list of features supported by the current platform. The array of
    length $(LREF DISCID_FEATURE_LENGTH) should be allocated by the user. After
    the call each element of the array is either null or a pointer to a static
    string.

    Params:
        features = a static string array of length $(LREF DISCID_FEATURE_LENGTH)
  +/
void discid_get_feature_list(scope ref char*[DISCID_FEATURE_LENGTH] features) @trusted pure nothrow @nogc;


/++
    Return the full version string of this library, including the name. This
    can be used for debug output. Don't use this to test for features, see
    $(LREF discid_has_feature).

    Returns: a C-string containing the version of libdiscid.
  +/
char* discid_get_version_string() @trusted pure nothrow @nogc;
