# libdiscid

These are the D bindings for the
[libdiscid](http://musicbrainz.org/doc/libdiscid) library from MusicBrainz.

Note that the PATCH portion of the version number for the bindings does not
necessarily match the actual C library. This is because the bindings may need
to be updated even if the C library isn't (e.g. if it were discovered that one
of the bindings was actually wrong). So, 0.6.x of the D bindings goes with
0.6.x of the C library, but the D bindings might be 0.6.0 when the C library is
actually 0.6.2. of the bindings was actually wrong). `DISCID_VERSION_PATCH` in
[discid.d](https://gitlab.com/jmdavisprog/libdiscid/blob/master/source/discid.d)
provides the patch version of the C library just like it does in
discid/discid.h. So, 0.6.x of the D bindings goes with 0.6.x of the C library,

The D documentation can be found [here](http://jmdavisprog.com/projects.html)
rendered as html, or it can be read from the source code.

The original documentation can be found [here](http://jonnyjd.github.io/libdiscid/)
